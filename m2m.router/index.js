var express = require('express')
var proxy = require('express-http-proxy')
const url = require('url')
var app = express()

app.use('/modlog', proxy(process.env.MODLOG_DNS_NAME))
app.use('/*', proxy(process.env.LOBSTERS_DNS_NAME, {
    proxyReqPathResolver: req => url.parse(req.baseUrl).path,
    preserveHostHdr: true
}))

app.listen(8085, () => console.log("Listening on 8085"))
