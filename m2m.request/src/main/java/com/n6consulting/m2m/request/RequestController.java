package com.n6consulting.m2m.request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/request")
public class RequestController {
  @Autowired
  private RequestRepository requestRepository;

  @RequestMapping(method = RequestMethod.GET)
  Iterable<Request> index() {
    return requestRepository.findAll();
  }
  
  @RequestMapping(method = RequestMethod.POST)
  ResponseEntity<?> add(@RequestBody Request input) {
    for (Evidence e : input.getEvidence()) {
      e.setRequest(input);
    }
    Request output = requestRepository.save(input);
    return ResponseEntity.ok(output);
  }

  // BEGIN ADD update-status
  enum Action implements Identifiable<String> {
    approve(Request.Status.approved),
    reject(Request.Status.rejected);

    private Request.Status targetStatus;

    Action(Request.Status status) {
      this.targetStatus = status;
    }

    Request doTo(Request r) {
      r.setStatus(targetStatus);
      return r;
    }

    @Override
    public String getId() {
      return this.name();
    }
  }

  @RequestMapping(path = "/{id}/{action}", method = RequestMethod.POST)
  ResponseEntity<?> action(@PathVariable(name = "id") Long id, @PathVariable("action") Action action, @RequestBody Notarization notarization) {
    Optional<Request> request = requestRepository.findById(id);

    request
      .map(action::doTo)
      .map(r -> r.notarize(notarization))
      .map(requestRepository::save);

    return ResponseEntity.ok(request);
  }
  // END ADD update-status

  // BEGIN ADD with-action-links
  private static void addAction(Action action, Resource<Request> resource) {
    resource.add(linkTo(RequestController.class).slash(resource.getContent().getId()).slash(action).withRel(action.name()));
  }

  public static void addActionLinks(Resource<Request> resource) {
    if (resource.getContent().isOpen()) {
      for (Action a : Action.values()) {
        addAction(a, resource);
      }
    }
  }
  // END ADD with-action-links
}
