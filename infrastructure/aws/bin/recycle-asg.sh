#! /bin/bash
ASG_NAME=$1

if [ -z "$ASG_NAME" ]
then
    echo "You must provide an ASG name as the only argument"
    exit 1
fi

INSTANCE_IDS=`aws autoscaling describe-auto-scaling-groups --auto-scaling-group-names ${ASG_NAME} --query 'AutoScalingGroups[*].Instances[*].{instance:InstanceId}' --output text`

for id in $INSTANCE_IDS
do
  aws autoscaling terminate-instance-in-auto-scaling-group --instance-id $id --no-should-decrement-desired-capacity
done

echo "Done."
exit 0