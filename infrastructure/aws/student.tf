# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = ""
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = ""
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = ""
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = ""
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = ""
}
