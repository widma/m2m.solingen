#cloud-config
write_files:
-   encoding: b64
    path:  /var/lib/lobsters/config/database.yml
    content: "${database_yml_content}"
    permissions: '0644'
-   encoding: b64
    path:  /var/lib/lobsters/config/environments/production.rb
    content: "${production_rb_content}"
    permissions: '0644'
-   encoding: b64
    path: /etc/telegraf/telegraf.d/telegraf.conf
    content: "${lobsters_telegraf_conf_content}"
    permissions: '0644'
-   encoding: b64
    path:  /var/lib/lobsters/config/initializers/influxdb-rails.rb
    content: "${influxdb_rails_rb_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli

runcmd:
-  wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
-  sudo dpkg -i telegraf_1.6.3-1_amd64.deb
-  apt update
-  apt install -y ruby-dev ruby-bundler rake build-essential libmysqlclient-dev libsqlite3-dev cmake nodejs
-  apt autoremove -y
-  gem install bundler
-  gem install execjs
-  AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${lobsters_service_bucket}/lobsters.tgz" /var/lib/lobsters/lobsters.tgz
-  chown -R ubuntu /home/ubuntu/lobsters
-  su -l -c "tar xzf /var/lib/lobsters/lobsters.tgz" ubuntu
-  su -l -c "cp -r /var/lib/lobsters/config/. /home/ubuntu/lobsters/config/" ubuntu
-  su -l -c "cd lobsters && bundle install && rails db:migrate RAILS_ENV=production && SECRET_KEY_BASE=${rails_secret_key} rails server -b 0.0.0.0 -e production" ubuntu
