InfluxDB::Rails.configure do |config|
    config.influxdb_database = "solingen"
    config.influxdb_username = "root"
    config.influxdb_password = "root"
    config.influxdb_hosts    = ["http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"]
    config.influxdb_port     = 8086
end
